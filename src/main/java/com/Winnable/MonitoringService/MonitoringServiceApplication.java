package com.Winnable.MonitoringService;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableDiscoveryClient
@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class MonitoringServiceApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(MonitoringServiceApplication.class)
            .web(WebApplicationType.REACTIVE)
            .run(args);
    }
}
